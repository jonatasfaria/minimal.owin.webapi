﻿using System;
using System.Data.Entity;
using Microsoft.Owin.Hosting;
using MinimalOwinWebApiSelfHost.Models;

namespace MinimalOwinWebApiSelfHost
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("args", args);

            // Set up and seed the database:
            Console.WriteLine("Initializing and seeding database...");
            Database.SetInitializer(new ApplicationDbInitializer());

            // Specify the URI to use for the local host:
            string baseUri = "http://localhost:3000";

            Console.WriteLine("Starting web Server...");

            WebApp.Start<Startup>(baseUri);

            Console.WriteLine("Server running at {0} - press Enter to quit. ", baseUri);

            Console.ReadLine();
        }
    }
}
