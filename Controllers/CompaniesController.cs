﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using MinimalOwinWebApiSelfHost.Models;

namespace MinimalOwinWebApiSelfHost.Controllers
{
    [RoutePrefix("api/companies")]
    public class CompaniesController : ApiController
    {
        ApplicationDbContext db = new ApplicationDbContext();

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(db.Companies);
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            var company = await db.Companies.FirstOrDefaultAsync(c => c.Id == id);

            if (company != null)
                return Ok(company);
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(Company company)
        {
            if (company != null)
            {
                db.Companies.Add(company);
                await db.SaveChangesAsync();
                return Created("Inserido com sucesso!", company);
            }

            return BadRequest();
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put(Company company)
        {
            if (company != null)
            {
                var entity = db.Companies.Find(company.Id);
                entity.Name = company.Name;
                await db.SaveChangesAsync();
                return Ok("Atualizado com sucesso!");
            }

            return BadRequest();
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var entity = db.Companies.Find(id);
            db.Companies.Remove(entity);
            await db.SaveChangesAsync();

            return Ok("Removido com sucesso!");
        }
    }
}
