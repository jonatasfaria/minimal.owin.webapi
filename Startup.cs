﻿using System.Web.Http;
using Owin;
using Swashbuckle.Application;

namespace MinimalOwinWebApiSelfHost
{
    public class Startup
    {
        public void Configuration(IAppBuilder pipeline)
        {
            pipeline.UseWebApi(this.GetConfiguration());
        }

        public HttpConfiguration GetConfiguration()
        {
            var config = new HttpConfiguration();

            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "Minimal.Owin.WebApi");
            }).EnableSwaggerUi();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            return config;
        }
    }
}
